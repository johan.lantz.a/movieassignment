﻿namespace MovieAssignmentAPI.DTO.MovieDTO
{
    public class DTO_ReadMovie
    {
        public int Id { get; set; }
        public int? FranchiseId { get; set; }

        public string? Title { get; set; }

        public string? Genre { get; set; }
        public DateTime? ReleaseDate { get; set; }

        public string? Director { get; set; }

        public string? ImageUrl { get; set; }

        public string? TrailerUrl { get; set; }

        public List<int>? Characters { get; set; }

    }
}

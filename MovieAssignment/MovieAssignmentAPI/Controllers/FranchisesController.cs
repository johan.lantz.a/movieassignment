﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieAssignmentAPI.Data;
using MovieAssignmentAPI.DTO.CharacterDTO;
using MovieAssignmentAPI.DTO.FranchiseDTO;
using MovieAssignmentAPI.DTO.MovieDTO;
using MovieAssignmentAPI.Models;

namespace MovieAssignmentAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieAssignmentContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MovieAssignmentContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Recieves all franchises.
        /// </summary>
        /// <returns>A list of franchises or a response status code</returns>
        [HttpGet] // GET: api/Franchises
        public async Task<ActionResult<IEnumerable<DTO_ReadFranchise>>> GetFranchises()
        {
            if (_context.Franchises == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<DTO_ReadFranchise>>(await _context.Franchises.Include(f => f.Movies).ToListAsync());
        }

        /// <summary>
        /// Recieves a specific franchise based on ID.
        /// </summary>
        /// <param name="id">Franchise ID</param>
        /// <returns>The specified franchise or a response status code</returns>
        [HttpGet("{id}")] // GET: api/Franchises/5
        public async Task<ActionResult<DTO_ReadFranchise>> GetFranchise(int id)
        {
            if (_context.Franchises == null)
            {
                return NotFound();
            }
            var franchise = await _context.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<DTO_ReadFranchise>(franchise);
        }

        /// <summary>
        /// Edits a specific franchise based on ID.
        /// </summary>
        /// <param name="id">Franchise ID</param>
        /// <param name="franchise">The specified franchise</param>
        /// <returns>Response status code</returns>
        [HttpPut("{id}")] // PUT: api/Franchises/5
        public async Task<IActionResult> PutFranchise(int id, DTO_EditFranchise franchise)
        {
            if (id != franchise.DTO_Id)
            {
                return BadRequest();
            }

            Franchise oldFranchise = _context.Franchises.Include(f => f.Movies).Single(f => f.Id == franchise.DTO_Id);


            List<Movie> tempMovies = new();


            foreach (Movie mov in oldFranchise.Movies)
            {
                mov.FranchiseId = null;
                _context.Update(mov);
            }
            foreach (int movieId in franchise.Movies)
            {
                if (await _context.Movies.FindAsync(movieId) == null)
                {
                    return BadRequest("A specified movie does not exist");
                }
                else
                {

                    Movie tempMovie = await _context.Movies.FindAsync(movieId);

                    tempMovie.FranchiseId = franchise.DTO_Id;

                    tempMovies.Add(tempMovie);
                    _context.Update(tempMovie);
                }
            }

            oldFranchise.Description = franchise.Description;
            oldFranchise.Movies = tempMovies;
            oldFranchise.Name = franchise.Name;

            _context.Update(oldFranchise);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        /// <summary>
        /// Creates a franchise based on input from body in swagger.
        /// </summary>
        /// <param name="franchiseDTO">Franchise data</param>
        /// <returns>Response status code for creating a franchise or errors.</returns>
        [HttpPost] // POST: api/Franchises
        public async Task<ActionResult<Franchise>> PostFranchise(DTO_CreateFranchise franchiseDTO)
        {
            if (_context.Franchises == null)
            {
                return Problem("Entity set 'MovieAssignmentContext.Franchises'  is null.");
            }
            Franchise franchise = _mapper.Map<Franchise>(franchiseDTO);

            List<Movie> movies = new();

            foreach (int movieId in franchiseDTO.Movies)
            {
                Movie movie = await _context.Movies.FindAsync(movieId);
                if (movie == null)
                {
                    return BadRequest("Specified movie does not exist in current database");
                }
                movies.Add(movie);
            }

            franchise.Movies = movies;
            await _context.Franchises.AddAsync(franchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, franchiseDTO);
        }

        /// <summary>
        /// Removes a specific franchise based on ID.
        /// </summary>
        /// <param name="id">Franchise ID.</param>
        /// <returns>Response status code</returns>
        [HttpDelete("{id}")] // DELETE: api/Franchises/5
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (_context.Franchises == null)
            {
                return NotFound();
            }
            var franchise = await _context.Franchises.Include(f => f.Movies).Select(f => f).Where(f => f.Id == id).SingleAsync();
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        
        private bool FranchiseExists(int id)
        {
            return (_context.Franchises?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        /// <summary>
        /// Accesses movies through franchises to manipulate the relationship between them.
        /// </summary>
        /// <param name="id">Franchise ID.</param>
        /// <param name="franchiseConnections">Movies in specified franchise</param>
        /// <returns></returns>
        [HttpPut("UpdateMoviesInFranchise/{id}")]
        public async Task<ActionResult<IEnumerable<DTO_EditFranchise>>> UpdateMoviesInFranchise(int id, List<int> franchiseConnections)
        {
            if (_context.Franchises == null)
            {
                return NotFound();
            }
            Franchise franchise = await _context.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == id);
            List<Movie> movies = new();

            foreach (int num in franchiseConnections)
            {
                if (await _context.Movies.FindAsync(num) == null)
                {
                    return NotFound();
                }
                else
                {
                    Movie movie = await _context.Movies.FindAsync(num);
                    movies.Add(movie);
                }
            }
            franchise.Movies = movies;
            _context.Update(franchise);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();

        }

        /// <summary>
        /// Recieves all movies from a specific franchise based on ID.
        /// </summary>
        /// <param name="id">Franchise ID.</param>
        /// <returns>A list of movies or a response status code</returns>
        [HttpGet("GetMoviesInFranchise/{id}")]
        public async Task<ActionResult<IEnumerable<DTO_ReadMovie>>> MoviesInFranchise(int id)
        {
            if (_context.Franchises == null)
            {
                return NotFound();
            }
            Franchise franchise = await _context.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == id);
            ICollection<Movie> movies = franchise.Movies;
            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<DTO_ReadMovie>>(movies);
        }


        /// <summary>
        /// Recieves all characters in a specific franchise based on ID.
        /// </summary>
        /// <param name="id">Franchise ID.</param>
        /// <returns>A list of characters or a response status code.</returns>
        [HttpGet("GetCharactersInFranchise/{id}")]
        public async Task<ActionResult<IEnumerable<DTO_ReadCharacter>>> CharactersInFranchise(int id)
        {
            if (_context.Franchises == null)
            {
                return NotFound();
            }
            Franchise franchise = await _context.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == id);
            ICollection<Movie> movies = franchise.Movies;
            List<Character> characters = new();

            if (franchise == null)
            {
                return NotFound();
            }

            foreach (Movie movie in movies)
            {
                Movie currentMovie = await _context.Movies.Include(m => m.Characters).FirstOrDefaultAsync(m => m.Id == movie.Id);
                foreach(Character character in currentMovie.Characters)
                {
                    characters.Add(character);
                }
            }
            return _mapper.Map<List<DTO_ReadCharacter>>(characters);

        }
    }
}

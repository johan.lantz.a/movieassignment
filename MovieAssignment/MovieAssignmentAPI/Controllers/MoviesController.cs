﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieAssignmentAPI.Data;
using MovieAssignmentAPI.DTO.CharacterDTO;
using MovieAssignmentAPI.DTO.MovieDTO;
using MovieAssignmentAPI.Models;

namespace MovieAssignmentAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MovieAssignmentContext _context;
        private readonly IMapper _mapper;
        public MoviesController(MovieAssignmentContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Recieves all movies.
        /// </summary>
        /// <returns>A list of movies or a response status code.</returns>
        [HttpGet] // GET: api/Movies
        public async Task<ActionResult<IEnumerable<DTO_ReadMovie>>> GetMovies()
        {
            if (_context.Movies == null)
            {
                return NotFound();
            }


            return _mapper.Map<List<DTO_ReadMovie>>(await _context.Movies.Include(m => m.Characters).ToListAsync());
        }

        /// <summary>
        /// Recieves a specific movie based on ID.
        /// </summary>
        /// <param name="id">Movie ID.</param>
        /// <returns>The specified movie or a response status code.</returns>
        [HttpGet("{id}")] // GET: api/Movies/5
        public async Task<ActionResult<DTO_ReadMovie>> GetMovie(int id)
        {
            if (_context.Movies == null)
            {
                return NotFound();
            }
            var movie = await _context.Movies.Include(m => m.Characters).FirstOrDefaultAsync(m => m.Id == id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<DTO_ReadMovie>(movie);
        }

        /// <summary>
        /// Edits a specific movie based on ID.
        /// </summary>
        /// <param name="id">Movie ID.</param>
        /// <param name="movie">The specified movie.</param>
        /// <returns></returns>
        [HttpPut("{id}")] // PUT: api/Movies/5
        public async Task<IActionResult> PutMovie(int id, DTO_EditMovie movie)
        {
            if (id != movie.DTO_Id)
            {
                return BadRequest();
            }

            Movie oldMovie = _context.Movies.Include(m => m.Characters).Single(m => m.Id == movie.DTO_Id);

            List<Character> tempCharacters = new();

            foreach (int CharacterId in movie.Characters)
            {
                
                if (await _context.Characters.FindAsync(CharacterId) == null)
                {
                    return BadRequest("A specified character for this update does not exist");
                }
                else
                {
                    Character character = await _context.Characters.FindAsync(CharacterId);
                    tempCharacters.Add(character);

                }
            }
            if (await _context.Franchises.FirstOrDefaultAsync(f => f.Id == movie.FranchiseId) == null)
            {
                return NotFound();
            }

            oldMovie.FranchiseId = movie.FranchiseId;
            oldMovie.Title = movie.Title;
            oldMovie.Genre = movie.Genre;
            oldMovie.ReleaseDate = movie.ReleaseDate;
            oldMovie.Director = movie.Director;
            oldMovie.ImageUrl = movie.ImageUrl;
            oldMovie.TrailerUrl = movie.TrailerUrl;
            oldMovie.Characters = tempCharacters;

            _context.Update(oldMovie);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Creates a movie based on input from body in swagger.
        /// </summary>
        /// <param name="movieDTO">Movie data.</param>
        /// <returns>Response status code for creating a movie or errors.</returns>
        [HttpPost] // POST: api/Movies
        public async Task<ActionResult<DTO_CreateMovie>> PostMovie(DTO_CreateMovie movieDTO)
        {
            if (_context.Movies == null)
            {
                return Problem("Entity set 'MovieAssignmentContext.Movies'  is null.");
            }
            Movie movie = _mapper.Map<Movie>(movieDTO);

            List<Character> Characters = new();

            foreach (int characterId in movieDTO.Characters)
            {
                Character character = await _context.Characters.FindAsync(characterId);
                if (character == null)
                {
                    return BadRequest("Specified character for this movie does not exist in the database.");
                }
                Characters.Add(character);
            }

            movie.Characters = Characters;

            await _context.Movies.AddAsync(movie);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovie", new { id = movie.Id }, movieDTO);
        }

        /// <summary>
        /// Removes a specific movie based on ID.
        /// </summary>
        /// <param name="id">Movie ID.</param>
        /// <returns>Response status code</returns>
        [HttpDelete("{id}")] // DELETE: api/Movies/5
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (_context.Movies == null)
            {
                return NotFound();
            }
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool MovieExists(int id)
        {
            return (_context.Movies?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        /// <summary>
        /// Accesses characters through movies to manipulate the relationship between them.
        /// </summary>
        /// <param name="id">Movie ID.</param>
        /// <param name="movieConnections">Characters in specified movie</param>
        /// <returns></returns>
        [HttpPut("UpdateCharacterInMovie/{id}")]
        public async Task<ActionResult<IEnumerable<DTO_EditMovie>>> UpdateCharactersInMovie(int id, List<int> movieConnections)
        {
            if (_context.Movies == null)
            {
                return NotFound();
            }
            Movie movie = await _context.Movies.Include(m => m.Characters).FirstOrDefaultAsync(m => m.Id == id);
            List<Character> characters = new();
            foreach (int num in movieConnections)
            {
                if (await _context.Characters.FindAsync(num) == null)
                {
                    return NotFound();
                }
                else
                {
                    Character character = await _context.Characters.FindAsync(num);
                    characters.Add(character);
                }
            }
            movie.Characters = characters;
            _context.Update(movie);
           
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        /// <summary>
        /// Recieves all characters from a specific movie based on ID.
        /// </summary>
        /// <param name="id">Movie ID.</param>
        /// <returns>A list of movies or a response status code</returns>
        [HttpGet("GetCharactersInMovie/{id}")]
        public async Task<ActionResult<IEnumerable<DTO_ReadCharacter>>> GetCharactersInMovie(int id)
        {
            if (_context.Movies == null)
            {
                return NotFound();
            }

            Movie movie = await _context.Movies.Include(m => m.Characters).FirstOrDefaultAsync(m => m.Id == id);
            ICollection<Character> characters = movie.Characters;

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<DTO_ReadCharacter>>(characters);
        }
    }
}

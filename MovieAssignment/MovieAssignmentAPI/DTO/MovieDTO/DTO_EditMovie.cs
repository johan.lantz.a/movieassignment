﻿using MovieAssignmentAPI.Models;
using System.ComponentModel.DataAnnotations;

namespace MovieAssignmentAPI.DTO.MovieDTO
{
    public class DTO_EditMovie
    {
        public int DTO_Id { get; set; }

        public int? FranchiseId { get; set; }

        [StringLength(100)]
        public string? Title { get; set; }

        [StringLength(100)]
        public string Genre { get; set; }
        public DateTime? ReleaseDate { get; set; }

        [StringLength(50)]
        public string? Director { get; set; }

        [StringLength(300)]
        public string? ImageUrl { get; set; }

        [StringLength(300)]
        public string? TrailerUrl { get; set; }

        public List<int>? Characters { get; set; }

    }
}

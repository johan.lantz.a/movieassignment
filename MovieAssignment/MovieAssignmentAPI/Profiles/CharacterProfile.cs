﻿using AutoMapper;
using MovieAssignmentAPI.DTO.CharacterDTO;
using MovieAssignmentAPI.Models;

namespace MovieAssignmentAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, DTO_ReadCharacter>()
                .ForMember(characterDTO => characterDTO.Movies, options => options
                .MapFrom(character => character.Movies.Select(movie => movie.Id).ToList()))
                .ReverseMap();



            CreateMap<DTO_CreateCharacter, Character>()
                .ForMember(character => character.Movies, options => options
                .MapFrom(characterDTO => new List<Movie>()))
                .ReverseMap();
        }
    }
}

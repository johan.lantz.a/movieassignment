﻿using System.ComponentModel.DataAnnotations;

namespace MovieAssignmentAPI.Models
{
    public class Franchise
    {

        public Franchise()
        {
         
        }

        public int Id { get; set; }

        public virtual ICollection<Movie>? Movies { get; set; }

        [StringLength(50)]
        public string? Name { get; set; }

        [StringLength(300)]
        public string? Description { get; set; }
    }
}

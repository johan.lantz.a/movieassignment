﻿using System.ComponentModel.DataAnnotations;

namespace MovieAssignmentAPI.DTO.CharacterDTO
{
    public class DTO_ReadCharacter
    {
        public int Id { get; set; }
        public string? Name { get; set; }

        public string? Alias { get; set; }

        public string? Gender { get; set; }

        public string? ImageUrl { get; set; }

        public List<int>? Movies { get; set; }
    }
}

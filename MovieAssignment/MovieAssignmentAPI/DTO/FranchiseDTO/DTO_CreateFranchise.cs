﻿using System.ComponentModel.DataAnnotations;

namespace MovieAssignmentAPI.DTO.FranchiseDTO
{
    public class DTO_CreateFranchise
    {

        [StringLength(50)]
        public string? Name { get; set; }

        [StringLength(300)]
        public string? Description { get; set; }

        public List<int>? Movies { get; set; }
    }
}


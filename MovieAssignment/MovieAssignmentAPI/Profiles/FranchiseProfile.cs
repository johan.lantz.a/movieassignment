﻿using AutoMapper;
using MovieAssignmentAPI.DTO.FranchiseDTO;
using MovieAssignmentAPI.Models;

namespace MovieAssignmentAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, DTO_ReadFranchise>()
                .ForMember(franchiseDTO => franchiseDTO.Movies, options => options
                .MapFrom(franchise => franchise.Movies.Select(movie => movie.Id).ToList()))
                .ReverseMap();



            CreateMap<DTO_CreateFranchise, Franchise>()
                .ForMember(franchise => franchise.Movies, options => options
                .MapFrom(franchiseDTO => new List<Movie>()))
                .ReverseMap();
        }
    }
}

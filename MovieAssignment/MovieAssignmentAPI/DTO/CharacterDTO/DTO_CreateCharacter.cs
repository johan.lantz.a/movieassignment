﻿using System.ComponentModel.DataAnnotations;

namespace MovieAssignmentAPI.DTO.CharacterDTO
{
    public class DTO_CreateCharacter
    {

        [StringLength(50)]
        public string? Name { get; set; }

        [StringLength(50)]
        public string? Alias { get; set; }

        [StringLength(50)]
        public string? Gender { get; set; }

        [StringLength(300)]
        public string? ImageUrl { get; set; }

        public List<int>? Movies { get; set; }
    }
}
